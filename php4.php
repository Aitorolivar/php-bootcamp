<?php
declare( strict_types = 1 );

function hola():void {
    echo "hola mundo";
}

function suma( int $num1, int $num2, int $num3=10):int{

    return $num1 + $num2 + $num3;

}

function fecha():string {
    return date( format:'d-m-Y');
}

$resultado = suma(num1:3, num2:5, num3:2);
echo "el resultado es $resultado";