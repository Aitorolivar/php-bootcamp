<?php

$paises = [
    "Italy" => "Rome",
    "Luxembourg" =>  "Luxembourg",
    "Belgium" => "Brussels",
    "Denmark" => "Copenhagen",
    "Finland" => "Helsinki",
    "France" => "Paris",
    "Slovakia" => "Bratislava",
    "Slovenia" => "Ljubljana",
    "Germany" => "Berlin",
    "Greece" => "Athens",
    "Ireland" => "Dublin",
    "Spain" => "Madrid",
    "Sweden" => "Stockholm",
    "Estonia" => "Tallin"
];

asort($paises);

foreach($paises as $pais=>$capital) {
    echo "$capital es la de $pais </br>";
}