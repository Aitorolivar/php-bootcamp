<?php
function mileanizar($texto){
    $textoconvertido= str_replace(
        ['que', 'porque', 'gu', 'bu', 'igual'],
        ['k', 'xq', 'w', 'w', '='],
        strtolower($texto)
    );
    return $textoconvertido;
};